/*
	Menu.h
	ver:0.1.0
	菜单源文件
	包含一些与菜单相关的函数声明
*/
#ifndef MENU_H_
#define	MENU_H_

/*————————头文件包含————————*/

#include "Graph.h"
#include "Control.h"
#include "Game.h"

/*————————函数的声明————————*/

void MainMenu(void);		//显示主菜单

void ExitGame(void);		//退出游戏菜单

void MenuSelect(int n);		//进入相应菜单

/*————————程序预处理————————*/

extern int g_menu_y[4];			//菜单项左上角y坐标
extern int g_menu_x;			//菜单项左上角x坐标
extern int g_menu_y_size;		//菜单项右下角相对左上角y坐标增量
extern int g_menu_x_size;		//菜单项右下角相对左上角x坐标增量
extern int g_menu;		    	//当前菜单所在位置
extern bool g_enter_menu;		//进入当前所在菜单

#endif // !MENU_H_
