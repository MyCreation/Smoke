/*
	Graph.h
	ver:0.1.0
	图形函数的头文件
	存放一些控制图形界面显示的函数的声明
*/

#ifndef GRAPH_H_
#define	GRAPH_H_


/*————————头文件包含————————*/

#include <graphics.h>
#include <time.h>

#include "Menu.h"
#include "Smoke.h"

/*————————库文件包含————————*/
#pragma comment(lib, "MSIMG32.LIB")

/*————————函数的声明————————*/

void ShowStart(void);											//显示程序的开始界面
void ShowMenuBK(void);											//显示菜单的背景图片
void ShowSelect(int num);										//显示菜单的选项框
void ShowTitle();												//显示游戏标题
void ShowGenius(IMAGE &imgA, IMAGE &imgB, int x, int y);		//显示精灵图		
void ClearTitlePlace(DWORD* imgPtr1, DWORD* imgPtr2);			//清除标题区域																
void ShowTeamName();												//显示队伍名
void CalcBKimg(WCHAR *s, int chapter);												//计算背景数据
void MoveBKimg(IMAGE &imgScoreBK, int score);												//移动背景图并从新计算数据
void ModifyHinderXY(int x0, int n);								//修改障碍物坐标
void ShowHinder();												//显示障碍物
void DelHinder();												//删除障碍物
void BuildNewHinder();											//判断能否再次生成障碍物
/*————————程序预处理————————*/
#define STIME 15			//程序开始渐隐动画sleep时间
#define MTIME 1				//程序菜单过渡动画sleep时间
#define Random(x) (rand() % x)
#define PicSX	250			//保存图片的x起始坐标
#define PicSY	140			//保存图片的y起始坐标


#define Loc(x,y)	(y * WIN_X + x)				//计算坐标位置
#define Loc2(x,y)	((y - PicSY) * 300 + x - PicSX)	//计算剪切图片的坐标
#define RDCOLOR(x)	(x > 255 ? 255 : x);		//重置颜色范围UP
#define RPCOLOR(x)	(x < 0 ? 0 : x);			//重置颜色范围DOWN
#define HY1			150							//开始检测鼻毛垂直位置的起始Y坐标
#define HY2			390							//开始检测鼻毛第二层垂直位置的Y坐标
#define HINDER_NUM	6							//同屏幕最多显示障碍物数目
#define CHECK_HINDER_NUM	215					//检测障碍的数量

#define DBLACK		0x000000					//黑色


typedef struct Hinder{
	int x1, y1;			//障碍物开始坐标
	int x2, y2;			//障碍物结束坐标
	bool	show;		//是否显示该鼻毛
	IMAGE	*img;		//存储被覆盖的像素信息
}Hinder;

extern Hinder g_hinder[HINDER_NUM];				//扩展障碍物的范围

//	定义PM2.5检测坐标的类
struct PMLocal
{
	int x;
	int y;
};

extern PMLocal	g_PML[CHECK_HINDER_NUM];		//存储应检测的坐标

#endif // !GRAPH_H_
