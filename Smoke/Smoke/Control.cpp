/*
	Colsole.cpp
	ver:0.1.0
	控制函数的源文件
	包含一些检测按键，控制移动的函数
*/
/*————————头文件包含————————*/
#include "Control.h"

/*————————函数的定义————————*/

//1000 0000 0000 0000 = 0x8000
//	获取实时键盘状态
int GetAsKeyState(void)
{
	int c = 0;
	//	获取按键的状态放到c里面
	if (GetAsyncKeyState(VK_UP)		& KEY_VALUE)	c |= KEY_UP;
	if (GetAsyncKeyState(VK_DOWN)	& KEY_VALUE)	c |= KEY_DOWN;
	if (GetAsyncKeyState(VK_LEFT)	& KEY_VALUE)	c |= KEY_LEFT;
	if (GetAsyncKeyState(VK_RIGHT)	& KEY_VALUE)	c |= KEY_RIGHT;
	if (GetAsyncKeyState(VK_RETURN)	& KEY_VALUE)	c |= KEY_ENTER;
	if (GetAsyncKeyState(VK_ESCAPE) & KEY_VALUE)	c |= KEY_ESC;

	return c;	
}

//	控制菜单选项框值
void KeyControl()
{
	bool flag = false;		//记录菜单有无改变
	if (GetAsyncKeyState(VK_UP)		& KEY_VALUE)
	{
		g_menu = g_menu - 1 == -1 ? 3 : g_menu - 1;
		flag = true;
	}
	if (GetAsyncKeyState(VK_DOWN)	& KEY_VALUE)
	{
		g_menu = g_menu + 1 == 4 ? 0 : g_menu + 1;
		flag = true;
	}
		
	if (GetAsyncKeyState(VK_ESCAPE)	& KEY_VALUE)	ExitGame();			//退出游戏
	if (GetAsyncKeyState(VK_RETURN)	& KEY_VALUE)	g_enter_menu = true;
	if (flag)
		Sleep(150);
}
//	鼠标控制
void MouseControl()
{
	MOUSEMSG m;
	if (MouseHit())
	{
		m = GetMouseMsg();
		switch (m.uMsg)
		{
		case WM_MOUSEMOVE:
			if (m.x >= g_menu_x && m.x <= g_menu_x + g_menu_x_size)
			{
				for (int i = 0; i < 4; i++)
				{
					if (m.y >= g_menu_y[i] && m.y <= g_menu_y[i] + g_menu_y_size)
						g_menu = i;
				}
			}
			break;
		case WM_LBUTTONDOWN:
			if (m.x >= g_menu_x && m.x <= g_menu_x + g_menu_x_size)
			{
				for (int i = 0; i < 4; i++)
				{
					if (m.y >= g_menu_y[i] && m.y <= g_menu_y[i] + g_menu_y_size)
					{
						g_enter_menu = true;
						break;
					}
				}
			}
			break;
		default:
			break;
		}
	//	Sleep(10);
	}
}
//	计算PM2.5坐标并返回是否有过移动
bool CalcPM2_5XY(void)
{
	bool move = false;
	int c = GetAsKeyState();
	if (c & KEY_UP && CheckMove(KEY_UP))
	{
			g_y -= STEP;
			move = true;
	}
	if (c & KEY_DOWN && CheckMove(KEY_DOWN))
	{
			g_y += STEP;
			move = true;
	}
	if (c & KEY_LEFT && CheckMove(KEY_LEFT))
	{
		g_x -= STEP;
		move = true;
	}
	if (c & KEY_RIGHT && CheckMove(KEY_RIGHT))
	{
		g_x += STEP;
		move = true;
	}
	return move;
}
//	检测pm2.5左右的移动
void CalcPM2_5LR()		
{
	int c = GetAsKeyState();
	if (c & KEY_LEFT)
		g_x -= STEP;
	if (c & KEY_RIGHT)
		g_x += STEP;
}
//	判断能否移动
bool CheckMove(int key)
{
	switch (key)
	{
	case KEY_UP:
		for (int i = 2; i < 5; i++)
		for (int j = 1; j < 5; j++)
		if (g_bkhaveimg[(g_y + g_PM_y[i]) + (g_x + g_PM_x[i]) * WIN_Y - j] == true)
			return false;
		return true;
		break;
	case KEY_DOWN:
		for (int i = 8; i < 11; i++)
		for (int j = 1; j < 5; j++)
		if (g_bkhaveimg[(g_y + g_PM_y[i]) + (g_x + g_PM_x[i]) * WIN_Y + j] == true)
			return false;
		return true;
		break;
	case KEY_LEFT:
		for (int j = 1; j < 5; j++)
		if (g_bkhaveimg[(g_y + g_PM_y[2]) + (g_x + g_PM_x[2] - j) * WIN_Y] == true)
			return false;
		for (int i = 8; i < 11; i++)
		for (int j = 1; j < 5; j++)
		if (g_bkhaveimg[(g_y + g_PM_y[9]) + (g_x + g_PM_x[9] - j) * WIN_Y] == true)
			return false;
		if (g_x <= 0)			//当触及边界
			return false;
		return true;
		break;
	case KEY_RIGHT:
		for (int j = 1; j < 5; j++)
		if (g_bkhaveimg[(g_y + g_PM_y[4]) + (g_x + g_PM_x[4] + j) * WIN_Y] == true)
			return false;
		for (int j = 1; j < 5; j++)
		if (g_bkhaveimg[(g_y + g_PM_y[8]) + (g_x + g_PM_x[8] + j) * WIN_Y] == true)
			return false;
		return true;
	}
	return true;
}
//	精确延时函数
void HpSleep(int ms)
{
	static clock_t oldclock = clock();		// 静态变量，记录上一次 tick

	oldclock += ms * CLOCKS_PER_SEC / 1000;	// 更新 tick

	if (clock() > oldclock)					// 如果已经超时，无需延时
		oldclock = clock();
	else
	while (clock() < oldclock)			// 延时
		Sleep(1);						// 释放 CPU 控制权，降低 CPU 占用率
}
//	检测能否删除障碍并修改障碍坐标
void CheckDelHinder(int &score)
{
	if (g_hinder->x1 < 4)
	{
		score += 5;
		DelHinder();
	}
		// 修改坐标
	for (int i = 0; i < HINDER_NUM && g_hinder[i].show; i++)
	{
		g_hinder[i].x1 -= STEP;
		g_hinder[i].x2 -= STEP;
	}
	//	若障碍物跑到最左边，让它消失


 }