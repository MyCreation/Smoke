/*
	Graph.cpp
	ver:0.1.0
	图形函数的cpp文件
	存放一些图形显示相关的函数
*/

/*————————头文件包含————————*/


#include "Graph.h"

/*————————程序预处理————————*/
Hinder g_hinder[HINDER_NUM];
PMLocal	g_PML[CHECK_HINDER_NUM];		//存储应检测的坐标


//显示程序的开始界面
void ShowStart()
{
	IMAGE startBkImg, imgTitleBK;		//标题位置背景
	loadimage(&startBkImg, _T("JPG"), _T("StartBK"));
	
	// 获取绘图窗口和 img 对象的显存指针
	DWORD* imgPtr1 = GetImageBuffer();
	DWORD* imgPtr2 = GetImageBuffer(&startBkImg);
	//	渐隐加载图片
	int r, g, b;
	for (int light = 1; light <= 128; light++)
	{
		for (int i = 0; i < WIN_X * WIN_Y; i++)
		{
			r = (int)(GetRValue(imgPtr2[i]) * light >> 7);
			g = (int)(GetGValue(imgPtr2[i]) * light >> 7);
			b = (int)(GetBValue(imgPtr2[i]) * light >> 7);
			imgPtr1[i] = RGB(r, g, b);
		}
		//刷新显存
		FlushBatchDraw();
		Sleep(STIME); 
	}

	getimage(&imgTitleBK, PicSX, PicSY, 300, 80);		//保存指定区域图片
	imgPtr1 = GetImageBuffer();
	imgPtr2 = GetImageBuffer(&imgTitleBK);	//获取保存图片指针

	ShowTitle();		//显示游戏标题

	Sleep(2000);

	//	渐隐清除标题
	ClearTitlePlace(imgPtr1, imgPtr2);

	ShowTeamName();		//显示队伍名字

	ClearTitlePlace(imgPtr1, imgPtr2);

	//	渐隐清除图片
	for (int light = 256; light >= 0; light--)
	{
		for (int i = 0; i < WIN_X * WIN_Y; i++)
		{
			r = (int)(GetRValue(imgPtr1[i]) * light >> 8);
			g = (int)(GetGValue(imgPtr1[i]) * light >> 8);
			b = (int)(GetBValue(imgPtr1[i]) * light >> 8);
			imgPtr1[i] = RGB(r, g, b);
		}
		//刷新显存
		FlushBatchDraw();
		Sleep(STIME);
		if (r == 0 && g == 0 && b == 0)
			break;
	}
}

//	显示游戏标题
void ShowTitle()
{
	//	显示游戏Title
	IMAGE TitleA, TitleB;
	loadimage(&TitleA, _T("JPG"), _T("TITLEA"));
	loadimage(&TitleB, _T("JPG"), _T("TITLEB"));
	ShowGenius(TitleA, TitleB, PicSX, PicSY);
}
//	显示队伍名
void ShowTeamName()
{
	//	显示队名
	IMAGE imgA, imgB;
	loadimage(&imgA, _T("JPG"), _T("DCA"));
	loadimage(&imgB, _T("JPG"), _T("DCB"));
	ShowGenius(imgA, imgB, PicSX, PicSY);
	Sleep(2000);
}
//	渐隐清除标题与队名区域
void ClearTitlePlace(DWORD* imgPtr1, DWORD* imgPtr2)
{
	int r, g, b;
	srand(time(0));

	for (int k = 128; k >= 0; k--)
	{
		for (int y = PicSY; y < 215; y++)
		{
			for (int x = PicSX; x < 545; x++)
			{
				//	内容不同即说明该位置为Title位置
				if (imgPtr1[Loc(x, y)] != imgPtr2[Loc2(x, y)])
				{
					if (k != 0)			//不能对0求余故分开处理
					{
						if (Random(10) > 5)
						{
							r = RDCOLOR((int)(GetRValue(imgPtr2[Loc2(x, y)]) + Random(k)));
						}
						else
						{
							r = RPCOLOR((int)(GetRValue(imgPtr2[Loc2(x, y)]) - Random(k)));
						}

						if (Random(10) > 5)
						{
							g = RDCOLOR((int)(GetGValue(imgPtr2[Loc2(x, y)]) + Random(k)));
						}
						else
						{
							g = RPCOLOR((int)(GetGValue(imgPtr2[Loc2(x, y)]) - Random(k)));
						}

						if (Random(10) > 5)
						{
							b = RDCOLOR((int)(GetGValue(imgPtr2[Loc2(x, y)]) + Random(k)));
						}
						else
						{
							b = RPCOLOR((int)(GetGValue(imgPtr2[Loc2(x, y)]) - Random(k)));
						}
					}
					else
					{
						r = (int)(GetRValue(imgPtr2[Loc2(x, y)]));
						g = (int)(GetGValue(imgPtr2[Loc2(x, y)]));
						b = (int)(GetBValue(imgPtr2[Loc2(x, y)]));
					}
					imgPtr1[Loc(x, y)] = RGB(r, g, b);
				}

			}
		}
		FlushBatchDraw();
		Sleep(10);
	}
}
//	显示精灵图
void ShowGenius(IMAGE &imgA, IMAGE &imgB, int x, int y)
{

	putimage(x, y, &imgB, SRCAND);			//B和背景图进行与运算
	putimage(x, y, &imgA, SRCPAINT);		//A和背景图进行或运算
}

//显示菜单的背景图片
void ShowMenuBK(void)
{
	IMAGE mainMenuBK;
	loadimage(&mainMenuBK, _T("JPG"), _T("MENUBACK"));

//	putimage(0, 0, &mainMenuBK);


	DWORD* imgPtr1 = GetImageBuffer();				//	窗口显存指针
	DWORD* imgPtr2 = GetImageBuffer(&mainMenuBK);	//	图片绘图指针

	//	控制图片中心旋转切换
	for (int i = 0; i < WIN_X / 2; i++)
	{
		//	原点右边
		for (int j = 0; j <= i && j < WIN_Y / 2; j++)
		{
			imgPtr1[WIN_X * (WIN_Y / 2 - 1) + WIN_X / 2 + i - j * WIN_X]
				= imgPtr2[WIN_X * (WIN_Y / 2 - 1) + WIN_X / 2 + i - j * WIN_X];
			imgPtr1[WIN_X * (WIN_Y / 2 - 1) + WIN_X / 2 + i + j * WIN_X]
				= imgPtr2[WIN_X * (WIN_Y / 2 - 1) + WIN_X / 2 + i + j * WIN_X];
		}
		//	原点下边
		for (int j = 0; j <= i && i < WIN_Y / 2; j++)
		{
			imgPtr1[WIN_X * (WIN_Y / 2 - 1) + WIN_X / 2 + WIN_X * i + j]
				= imgPtr2[WIN_X * (WIN_Y / 2 - 1) + WIN_X / 2 + WIN_X * i + j];
			imgPtr1[WIN_X * (WIN_Y / 2 - 1) + WIN_X / 2 + WIN_X * i - j]
				= imgPtr2[WIN_X * (WIN_Y / 2 - 1) + WIN_X / 2 + WIN_X * i - j];
		}
		//	原点左边
		for (int j = 0; j <= i && j < WIN_Y / 2; j++)
		{
			imgPtr1[WIN_X * (WIN_Y / 2 - 1) + WIN_X / 2 - i + WIN_X * j]
				= imgPtr2[WIN_X * (WIN_Y / 2 - 1) + WIN_X / 2 - i + WIN_X * j];
			imgPtr1[WIN_X * (WIN_Y / 2 - 1) + WIN_X / 2 - i - WIN_X * j]
				= imgPtr2[WIN_X * (WIN_Y / 2 - 1) + WIN_X / 2 - i - WIN_X * j];
		}
		//	原点上边
		for (int j = 0; j <= i && i < WIN_Y / 2; j++)
		{
			imgPtr1[WIN_X * (WIN_Y / 2 - 1) + WIN_X / 2 - WIN_X * i - j]
				= imgPtr2[WIN_X * (WIN_Y / 2 - 1) + WIN_X / 2 - WIN_X * i - j];
			imgPtr1[WIN_X * (WIN_Y / 2 - 1) + WIN_X / 2 - WIN_X * i + j]
				= imgPtr2[WIN_X * (WIN_Y / 2 - 1) + WIN_X / 2 - WIN_X * i + j];
		}
		FlushBatchDraw();
//		Sleep(MTIME);
	}
}
//显示菜单的选项框
void ShowSelect(int num)
{
	//处理坐标
	int x1, y1, x2, y2;
	x1 = g_menu_x;
	x2 = x1 + g_menu_x_size;
	y1 = g_menu_y[num];
	y2 = y1 + g_menu_y_size;
	//	画框
	roundrect(x1, y1, x2, y2, 20, 20);
	roundrect(x1 - 1, y1 - 1, x2 + 1, y2 + 1, 20, 20);
	roundrect(x1 + 1, y1 + 1, x2 - 1, y2 - 1, 20, 20);
}
//	设置第一关的背景数据
void SetBK1Value(DWORD *imgPtr)
{
	if (!g_bkhaveimg)
		g_bkhaveimg = new bool[WIN_X * WIN_Y];		//动态分配内存
	if (!g_bkhinder)
		g_bkhinder = new bool[WIN_X * WIN_Y];

	memset(g_bkhinder, false, sizeof(bool)* WIN_Y * WIN_X);
	for (int x = 0; x < WIN_X; x++)
	{
		for (int y = 0; y < WIN_Y; y++)
		{
			//若显存对应位置为黑色，置true
			if (imgPtr[y * WIN_X + x] == DBLACK)
				g_bkhaveimg[x * WIN_Y + y] = true;
			else
				g_bkhaveimg[x * WIN_Y + y] = false;
		}
	}
}
//	设置第二关的背景数据
void SetBK2Value(DWORD *imgPtr)
{
	//	重新分配内存空间
	g_bkhaveimg = new bool[WIN_X*WIN_Y * 10];
	for (int i = 0; i < WIN_X * WIN_Y * 10; i++)
	if (imgPtr[i] == DBLACK)
		g_bkhaveimg[i] = true;
	else
		g_bkhaveimg[i] = false;

	g_bkhvimgbk = g_bkhaveimg;		//备份数据
}

//	计算关卡背景数据
void CalcBKimg(WCHAR *s,int chapter)		//存储背景图名字和关卡数
{
	IMAGE imgWB;
//	loadimage(&imgWB, L"JPG", L"CHAPTER1BW");
	loadimage(&imgWB, L"JPG", s);				//读取指定关卡的背景数据图以计算哪里能够移动
	

	DWORD *imgPtr = GetImageBuffer(&imgWB);	//获取黑白图片指针
	switch (chapter)			//针对不同关卡取不同行动
	{
	case 1:
		SetBK1Value(imgPtr);
		break;
	case 2:
		SetBK2Value(imgPtr);
		break;
	case 3:
		break;
	default:
		break;
	}

}

void ShowScore(IMAGE &imgScoreBK, int score)
{
	//	备份数据
	getimage(&imgScoreBK, 750, 0, 50, 50);
	WCHAR *s = new WCHAR[20];
	_itow(score, s, 10);
	outtextxy(750, 0, s);
}

//	移动背景
void MoveBKimg(IMAGE &imgScoreBK, int score)
{
	//恢复背景
	putimage(750, 0, &imgScoreBK);

	//	移动背景
	IMAGE imgMove, imgNoMove;
	getimage(&imgMove, 0, 0, STEP, WIN_Y);
	getimage(&imgNoMove, STEP, 0, WIN_X, WIN_Y);
	putimage(0, 0, &imgNoMove);
	putimage(WIN_X - STEP , 0, &imgMove);

	//	修改背景数据
	bool *tmp = new bool[WIN_X * WIN_Y];
	memcpy(tmp, &g_bkhaveimg[WIN_Y * STEP], WIN_Y * (WIN_X - STEP));
	memcpy(&tmp[(WIN_X - STEP) * WIN_Y], g_bkhaveimg, WIN_Y * STEP);
	delete[] g_bkhaveimg;
	g_bkhaveimg = tmp;
	//	修改障碍背景数据
	tmp = new bool[WIN_X * WIN_Y];
	memcpy(tmp, &g_bkhinder[WIN_Y * STEP], WIN_Y * (WIN_X - STEP));
	memcpy(&tmp[(WIN_X - STEP) * WIN_Y], g_bkhinder, WIN_Y * STEP);
	delete[] g_bkhinder;
	g_bkhinder = tmp;
	//	显示分数
	ShowScore(imgScoreBK, score);
}
//	移动第二关背景
void MoveBKimgCP2(DWORD *imgBKPtr,DWORD *imgMPtr)
{
	memcpy(imgMPtr, imgBKPtr, sizeof(DWORD)* WIN_X * WIN_Y);
}

//	显示障碍物
void ShowHinder()
{
	srand(time(0));
	int n = 0;
	for (int i = 400; i <= 800;i += Random(50) + 130)
	{
		ModifyHinderXY(i, n++);
	}
	for (; n < HINDER_NUM; n++)
		g_hinder[n].show = false;

}
//	修改障碍物的坐标
void ModifyHinderXY(int x0, int n)
{
	
	int i;
	Hinder *p = &g_hinder[n];
	//	为x赋初值
	p->x1 = x0;
	p->x2 = x0;

	//	随机生成上部或者下部的障碍
	if (Random(10) > 5)			
	{
		//	为y赋初值,障碍在上部
		for (i = WIN_Y * p->x1 + HY1; g_bkhaveimg[i]; i++);
		p->y1 = i - WIN_Y* p->x1;

		for (i = WIN_Y * p->x1 + HY2; !g_bkhaveimg[i]; i++);
		p->y2 = i - WIN_Y * p->x1 - Random(g_CreateHinderHighRandNum) - g_CreateHinderHigh;
	}
	else
	{
		//	为y赋初值,障碍在下部
		for (i = WIN_Y * p->x1 + HY1; g_bkhaveimg[i]; i++);
		p->y1 = i - WIN_Y* p->x1 + Random(g_CreateHinderHighRandNum) + g_CreateHinderHigh;

		for (i = WIN_Y * p->x1 + HY2; !g_bkhaveimg[i]; i++);
		p->y2 = i - WIN_Y * p->x1;
	}

	//	设置是否显示该障碍
	p->show = true;
	p->img = new IMAGE;

	getimage(p->img, p->x1, p->y1, p->x2 - p->x1 + 1, p->y2 - p->y1 + 1);
	memset(&g_bkhinder[p->x1 * WIN_Y + p->y1], true, sizeof(bool)* (p->y2 - p->y1 + 1));
	line(p->x1, p->y1, p->x2, p->y2);
}
//	删除障碍物
void DelHinder()
{
	Hinder *p = g_hinder;

	//	将障碍从图片上删除
	putimage(p->x1, p->y1, p->img);
	delete p->img;
	memset(&g_bkhinder[p->x1 * WIN_Y + p->y1], false, sizeof(bool)* (p->y2 - p->y1 + 1));

//	FlushBatchDraw();
	for (p++; p->show; p++)
	{
		memcpy(p - 1, p, sizeof(Hinder));
	}
	(p - 1)->show = false;

}
//	生成新的障碍物
void BuildNewHinder()
{
	//	找到最后一个障碍物
	int i;
	for (i = 0; g_hinder[i].show; i++);
	//Hinder *p = &g_hinder[--i];

	//	判断能否再生成一个障碍物
	if (WIN_X - g_hinder[i - 1].x1 >= 180)
	{
		ModifyHinderXY(g_hinder[i - 1].x1 + (50) + 120, i);
	}
}
//	初始化第二关
//void Init;