/*
	InitProgram.cpp
	ver:0.1.0
	存放一些初始化的函数
*/

/*————————头文件包含————————*/

#include "InitProgram.h"

/*————————函数的定义————————*/

void InitGraphics(void)
{
	initgraph(WIN_X, WIN_Y);
	// 设置输出效果为抗锯齿 (VC6 / VC2008 / VC2010 / VC2012)
	LOGFONT f;
	gettextstyle(&f);						//	获取当前字体设置
	f.lfHeight = 30;						//	设置字体高度为 30
	_tcscpy_s(f.lfFaceName, _T("黑体"));    //	设置字体为“黑体”
	f.lfQuality = ANTIALIASED_QUALITY;		//	设置输出效果为抗锯齿  
	settextstyle(&f);						//	设置字体样式
	settextcolor(YELLOW);					//	设置字体颜色为黑色

	//设置背景颜色，之后用背景色填充背景
	setbkcolor(RGB(210, 210, 210));	
	cleardevice();

	setbkmode(TRANSPARENT);			//设置图片与文字背景为透明

	//	设置窗口标题
	HWND hwnd = GetHWnd();
	SetWindowText(hwnd, _T("我是PM2.5")); 
	//加载鼠标
	HMODULE hmod = GetModuleHandle(NULL);				// 获取当前进程的句柄
	HCURSOR hcur = LoadCursor(hmod, _T("MOUSE"));	// 加载资源中的鼠标样式图片

	SetClassLong(hwnd, GCL_HCURSOR, (long)hcur);		 // 设置绘图窗口内的鼠标样式

	setlinecolor(YELLOW);
}

//	初始化Chapter1
void InitChapter1()
{
	CalcBKimg(L"CHAPTER1BW",1);		//计算背景数据
	loadimage(NULL, L"JPG", L"CHAPTER1BK");

	setlinecolor(BLACK);
	g_x = 50;
	g_y = WIN_Y / 2 - 50;

}
//	初始化第二关
void InitChapter2()
{
	CalcBKimg(g_Chapter2BW, 2);		//计算背景数据
	IMAGE img;
	loadimage(NULL, L"JPG", g_Chapter2BK);
	g_x = WIN_X / 2 - 100 / 2;
	g_y = 0;
}

//	初始化游戏
void InitGame()
{
	IMAGE img;
	//	计算在判断游戏结束时需要判断哪些点的坐标
	loadimage(&img, L"JPG", L"PMBK");
	DWORD *imgPtr = GetImageBuffer(&img);
	int n = 0, i;

	for (int y = 0; y < 100; y++)
	{
		for (int x = 0; x < 100; x++)
		{
			i = y * 100 + x;
			if (imgPtr[i] == 0x000000)
			{
				g_PML[n].x = x;
				g_PML[n].y = y;
				n++;
			}
		}
	}
}
//	释放指针
void InitExitValue()
{
	if (g_bkhaveimg)
	{
		delete[] g_bkhaveimg;
		g_bkhaveimg = NULL;
	}

	if (g_bkhinder)
	{
		delete[] g_bkhinder;
		g_bkhinder = NULL;
	}

}





