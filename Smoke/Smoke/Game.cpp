/*
	Game.cpp
	ver:0.1.0
	游戏主体框架
	存放一些游戏主体的函数
*/

/*————————头文件包含————————*/

#include "Game.h"

/*————————程序预处理————————*/

bool *g_bkhaveimg = NULL;			//背景数据指针
bool *g_bkhinder  = NULL;			//障碍物位置指针
bool *g_bkhvimgbk;					//背景数据指针备份
int g_x, g_y;				//PM2.5所在位置坐标
int g_PM_x[11] = { 0, 14, 19, 50, 77, 96, 100, 94, 85, 35, 12 };
int g_PM_y[11] = { 53, 32, 7, 8, 0, 23, 42, 67, 92, 100, 83 };
wchar_t *g_Chapter2BK = NULL;
wchar_t *g_Chapter2BW = NULL;
int g_CreateHinderHighRandNum = 0;	//随机生成障碍物中数值范围
int g_CreateHinderHigh = 0;			//生成障碍物固定距离


/*————————函数的定义————————*/
//	选择游戏难度		返回flase表示返回主菜单
bool GameModeSelete()
{
	g_enter_menu = false;
	if (!g_Chapter2BK)
		g_Chapter2BK = new wchar_t[15];
	if (!g_Chapter2BW)
		g_Chapter2BW = new wchar_t[15];
	switch (g_menu)
	{
	case 0:					//简单难度背景图
		wcscpy(g_Chapter2BK, _T("CHAPTER2BK1"));
		wcscpy(g_Chapter2BW, _T("CHAPTER2BW1"));
		g_CreateHinderHighRandNum = 40;
		g_CreateHinderHigh = 130;
		break;
	case 1:					//一般难度背景图
		wcscpy(g_Chapter2BK, _T("CHAPTER2BK2"));
		wcscpy(g_Chapter2BW, _T("CHAPTER2BW2"));
		g_CreateHinderHighRandNum = 30;
		g_CreateHinderHigh = 120;
		break;
	case 2:					//困难难度背景图
		wcscpy(g_Chapter2BK, _T("CHAPTER2BK3"));
		wcscpy(g_Chapter2BW, _T("CHAPTER2BW3"));
		g_CreateHinderHighRandNum = 10;
		g_CreateHinderHigh = 115;
		break;
	case 3:
		return false;
		break;
	}
	return true;
}
//	选择游戏难度		true表示正常游戏流程，false表示返回主菜单
bool ChooseGameMode()
{
	Sleep(150);
	//加载图片
	loadimage(NULL, _T("JPG"), _T("GAMEMODE"));
	//	显示选项框
	g_menu = 0;
	ShowSelect(g_menu);		//在第一个菜单上显示选项框
	int _menu = g_menu;
	while (1)
	{
		KeyControl();			//统计按键状态，计算菜单应在的地方
		MouseControl();			//统计鼠标状态
		if (_menu != g_menu)
		{
			_menu = g_menu;
			loadimage(NULL, _T("JPG"), _T("GAMEMODE"));		//	重新加载图片，清除上一状态
			ShowSelect(g_menu);
		}

		if (g_enter_menu)	//进入相应功能选单
		{
			if (GameModeSelete())
				return true;
			else
				return false;
		}
	}
}
//	开始游戏
void StartGame()
{
	if (!ChooseGameMode())
		return;
	InitGame();			//初始化游戏
	if (Chapter1() == GAMEOVER)
		return;
	if (Chapter2() == GAMEOVER)
	{
		return;
	}
}


void Chapter1GameOver(void)
{
	MessageBox(GetHWnd(), _T("进入人体失败!"), _T("提示"), MB_OK);
}
//	时间 分数
void Score(clock_t finish)
{
	static clock_t start = clock();
	WCHAR s[20] = L"我是PM2.5 - time:";
	int n;
	if (finish < start)
		finish = clock();
	int tmp = finish - start;
	if (tmp == 0)
		n = 1;
	while (tmp)
	{
		tmp /= 10;
		n++;
	}
	tmp = (finish - start) / 1000;
	WCHAR s1[20];
	_itow(tmp, s1, n);
	wcscat(s, s1);
	
	SetWindowText(GetHWnd(), s);
}
//	第一关
int Chapter1(void)
{
	
	InitChapter1();								//初始化
	int score = 0;								//分数

	IMAGE imgBK, imgPM2_5, imgPM2_5_BK, imgScoreBK;
	getimage(&imgBK, 0, 0, WIN_X, WIN_Y);		//备份当前背景图
	int imgPM2_5_BK_x = g_x, imgPM2_5_BK_y = g_y;
	getimage(&imgPM2_5_BK, g_x, g_y, 100, 100);
	loadimage(&imgPM2_5, L"JPG", L"PM2_5");
	putimage(g_x, g_y, &imgPM2_5, SRCAND);
	BeginBatchDraw();							//开始批量绘图
	ShowHinder();
	FlushBatchDraw();		//刷新绘图界面
	while (1)
	{
		if (CalcPM2_5XY())
		{
			//	当碰到障碍物
			if (CheckHinder())
			{
				Chapter1GameOver();
				EndBatchDraw();				//结束批量绘图
				return GAMEOVER;		//游戏结束标识
			}

			putimage(imgPM2_5_BK_x, imgPM2_5_BK_y, &imgPM2_5_BK);			//清除上次痕迹
			if (g_x > WIN_X / 2 - 150)										//若PM2.5位于窗口中部
			{
				BuildNewHinder();
				CheckDelHinder(score);											//若障碍走到最左边让其消失
				
				MoveBKimg(imgScoreBK,score);
				g_x -= STEP;
			}
			getimage(&imgPM2_5_BK, g_x, g_y, 100, 100);						//
			putimage(g_x, g_y, &imgPM2_5, SRCAND);							//在新位置绘图
			imgPM2_5_BK_x = g_x;
			imgPM2_5_BK_y = g_y;
			FlushBatchDraw();		//刷新绘图界面
			if (score >= 100)
			{
				EndBatchDraw();				//结束批量绘图
				return NEXTCP;				//进入下一关
			}

			HpSleep(15);
		}
	}
	
}


//	检测是否碰到障碍物
bool CheckHinder()
{
	for (PMLocal *p = g_PML; p < g_PML + CHECK_HINDER_NUM; p++)
	{
		if (g_bkhinder[(p->x + g_x) * WIN_Y + p->y + g_y])
			return true;
	}
	return false;
}
//	检测是否碰壁 - 第二关 若碰壁返回true
bool CheckCP2()
{
	//	定义指针指向几个外侧的点
	int *x = g_PM_x, *y = g_PM_y;
	//	循环判断是否碰壁
	for (; x - g_PM_x < 10; x++, y++)
	{
		if (g_bkhaveimg[(g_y + *y) * WIN_X + g_x + *x])
			return true;
	}
	return false;		//未碰壁
}

//	第二关
int Chapter2(void)
{
	InitChapter2();
	//	获取pm2.5所在位置背景图
	IMAGE imgPM2_5_BK, imgPM2_5, imgBK;
	int imgPM2_5_BK_x = g_x, imgPM2_5_BK_y = g_y;
	getimage(&imgPM2_5_BK, imgPM2_5_BK_x, imgPM2_5_BK_y, 100, 100);

	//	显示PM2.5
	loadimage(&imgPM2_5, L"JPG", L"PM2_5");
	putimage(g_x, g_y, &imgPM2_5, SRCAND);

	//	读取背景图
	loadimage(&imgBK, L"JPG", g_Chapter2BK);

	DWORD *imgBKPtr, *imgMPtr;					//定义绘图指针和显存指针
	imgBKPtr = GetImageBuffer(&imgBK);			//获取背景图片指针
	imgMPtr = GetImageBuffer();					//获取显存指针

	BeginBatchDraw();							//开始批量绘图
	int n = 0;

	while (1)
	{
		//	检测是否碰壁
		if (CheckCP2())
		{
			Chapter1GameOver();
			EndBatchDraw();				//结束批量绘图
			g_bkhaveimg = g_bkhvimgbk; //恢复数据
			return GAMEOVER;		//游戏结束标识
		}
		putimage(imgPM2_5_BK_x, imgPM2_5_BK_y, &imgPM2_5_BK);			//清除上次痕迹
		CalcPM2_5LR();							//检测按键的左右移动
		n += 4;
		if (g_y < WIN_Y / 2 - 50 || n >= 5650)
			g_y += STEP;
		else
		{
			//	控制背景移动
			imgBKPtr = &imgBKPtr[WIN_X * STEP];
			memcpy(imgMPtr, imgBKPtr, sizeof(DWORD)* WIN_X * WIN_Y);
			//	控制背景数据移动
			g_bkhaveimg = &g_bkhaveimg[WIN_X * STEP];
		}
		getimage(&imgPM2_5_BK, g_x, g_y, 100, 100);						//
		putimage(g_x, g_y, &imgPM2_5, SRCAND);							//在新位置绘图
		imgPM2_5_BK_x = g_x;
		imgPM2_5_BK_y = g_y;
		FlushBatchDraw();		//刷新绘图界面
		HpSleep(15);
		if (n >= 5900)
		{
			break;
		}
	}
	EndBatchDraw();
	g_bkhaveimg = g_bkhvimgbk; //恢复数据
	return 1;
}