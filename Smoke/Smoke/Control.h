/*
	Control.h
	ver:0.1.0
	控制函数头文件
*/



#ifndef CONTROL_H_
#define CONTROL_H_

/*————————头文件包含————————*/

#include "Graph.h"
#include "Menu.h"
#include "Game.h"

/*————————程序预处理————————*/
#define	KEY_VALUE		0x8000
#define	KEY_UP			1
#define	KEY_DOWN		2
#define	KEY_LEFT		4
#define	KEY_RIGHT		8
#define KEY_ENTER		16
#define	KEY_ESC			32

/*————————函数的声明————————*/

//1000 0000 0000 0000 = 0x8000
//	获取实时键盘状态
int GetAsKeyState(void);
void KeyControl();		//控制菜单选项框值
void MouseControl();	//鼠标控制
bool CheckMove(int key);	//判断能否移动
bool CalcPM2_5XY(void);		//计算PM2.5坐标并返回是否有过移动
void HpSleep(int ms);		//精确延时函数
void CheckDelHinder(int &score);		//判断并修改障碍位置并消失
void CalcPM2_5LR();				//检测PM2.5的左右移动

#endif // !CONTROL_H_
